package com.etsmtl.gestionrapide.Core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Communication;
import com.etsmtl.gestionrapide.Model.Facturation;
import com.etsmtl.gestionrapide.Model.Note;

public class Controller {

	private static Controller _clientController = null;
	private int activeClientPos = -1;

	private Controller() {
		initRepo();
	}

	private void initRepo() {

		Client c1 = new Client();
		c1.setCompagnyName("Un garage");
		c1.setFirstName("Marc-Antoine");
		c1.setLastName("Lemieux");
		c1.setEmailAddress("meurs@marcantoinelemieux.com");
		addClient(c1);

		Client c2 = new Client();
		c2.setCompagnyName("ETS");
		c2.setFirstName("Aleksandre");
		c2.setLastName("Clavet");
		c2.setEmailAddress("noreply@clavet.com");
		addClient(c2);

		Client c3 = new Client();
		c3.setCompagnyName("clubber inc.");
		c3.setFirstName("Jean-Christophe");
		c3.setLastName("Fortin");
		c3.setEmailAddress("jc@hotmail.com");
		addClient(c3);

		Note n1 = new Note();
		n1.setSubject("Note1");
		n1.setClient(c1);
		n1.setDate(new Date());

		Note n2 = new Note();
		n2.setSubject("Note2");
		n2.setClient(c2);
		n2.setDate(new Date());

		Facturation f1 = new Facturation();
		f1.setName("ui design");
		f1.setClient(c1);
		c1.addBill(f1);

		Facturation f2 = new Facturation();
		f2.setName("dépannage");
		f2.setClient(c2);
		c2.addBill(f2);

		Communication cm1 = new Communication();
		cm1.setDate(new Date());
		cm1.setType("SMS");
		cm1.setClient(c1);
		c1.addCommunication(cm1);

		Communication cm2 = new Communication();
		cm2.setDate(new Date());
		cm2.setType("phone call");
		cm2.setClient(c2);
		c2.addCommunication(cm1);
	}

	public static Controller getInstance() {
		if (_clientController == null) {
			_clientController = new Controller();
		}
		return _clientController;
	}

	private List<Client> _clients = new ArrayList<Client>();

	public List<Client> getClients() {
		return _clients;
	}

	public Client getClient(int id) {
		for (Client client : _clients) {
			if (client.hasId(id)) {
				return client;
			}
		}
		return null;
	}

	public void addClient(Client client) {
		if (client == null)
			return;

		client.setID(GetNextID());
		_clients.add(client);
	}

	private int GetNextID() {
		int maxID = 0;
		for (Client client : _clients) {
			if (client.getID() > maxID) {
				maxID = client.getID();
			}
		}
		return maxID + 1;
	}

	public void saveClient(Client modifiedClient) {
		int idxOfClient = -1;

		for (Client client : _clients) {
			if (client.equals(modifiedClient)) {
				idxOfClient = _clients.indexOf(client);
			}
		}

		if (idxOfClient > -1 && idxOfClient < _clients.size()) {
			_clients.remove(idxOfClient);
			_clients.add(modifiedClient);
		}
	}

	public void setActiveClient(Client client) {
		activeClientPos = _clients.indexOf(client);
	}

	public Client getActiveClient() {
		if (activeClientPos > -1 && activeClientPos < _clients.size()) {
			return _clients.get(activeClientPos);
		}
		return null;
	}

	public List<Note> getAllNotes() {
		List<Note> notes = new ArrayList<Note>();
		for (Client client : _clients) {
			for (Note note : client.getNotes()) {
				notes.add(note);
			}
		}
		return notes;
	}

	private Note _activeNote = null;

	public void setActiveNote(Note note) {
		this._activeNote = note;
	}

	public Note getActiveNote() {
		return _activeNote;
	}

	public List<Facturation> getAllBills() {
		List<Facturation> entries = new ArrayList<Facturation>();
		for (Client client : _clients) {			
			 entries.addAll(client.getBills());			 
		}
		return entries;
	}

	private Facturation _activeTimeEntry = null;

	public void setActiveTimeEntry(Facturation entry) {
		this._activeTimeEntry = entry;
	}

	public Facturation getActiveTimeEntry() {
		return _activeTimeEntry;
	}

	private Communication _activeCommunication = null;

	public void setActiveCommunication(Communication entry) {
		this._activeCommunication = entry;
	}

	public Communication getActiveCommunication() {
		return _activeCommunication;
	}

	public List<Communication> getAllCommunications() {
		List<Communication> coms = new ArrayList<Communication>();
		for (Client client : _clients) {
			coms.addAll(client.getCommunications());
		}
		return coms;
	}

	public void removeClient(Client _client) {
		_clients.remove(_client);
	}
}
