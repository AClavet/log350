package com.etsmtl.gestionrapide.Model;

import java.util.Date;

public class Communication {

	private String _type;
	private Date _date;
	private Client _client;
	private String _details;
	/**
	 * @return the _type
	 */
	public String getType() {
		return _type;
	}
	/**
	 * @param _type the _type to set
	 */
	public void setType(String _type) {
		this._type = _type;
	}
	/**
	 * @return the _date
	 */
	public Date getDate() {
		return _date;
	}
	/**
	 * @param _date the _date to set
	 */
	public void setDate(Date _date) {
		this._date = _date;
	}
	public Client getClient() {
		return _client;
	}
	public void setClient(Client _client) {
		this._client = _client;
	}
	public String getDetails() {
		return _details;
	}
	public void setDetails(String _details) {
		this._details = _details;
	}
}
