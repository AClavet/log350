package com.etsmtl.gestionrapide.Model;

public class Facturation {

	private String _name;
	private Client _client;
	private long msBilled = 0;

	public long getBilledTimeInMs() {
		return msBilled;
	}

	public String getName() {
		return _name;
	}

	public void setName(String _name) {
		this._name = _name;
	}

	public Client getClient() {
		return _client;
	}

	public void setClient(Client _client) {
		this._client = _client;
	}

	public void addTime(long timeInMs) {
		msBilled += timeInMs;
	}

	public boolean isOn() {
		// TODO Auto-generated method stub
		return false;
	}
}
