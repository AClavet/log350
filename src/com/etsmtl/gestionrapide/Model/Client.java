package com.etsmtl.gestionrapide.Model;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;

/**
 * 
 * This class represent a Client in the context of the application GestionRapide
 * 
 * @author: Jean-Christophe Fortin
 */

public class Client {

	private int _id = -1;
	private String _lastName;
	private String _firstName;
	private String _compagnyName;
	private String _emailAddress;
	private Bitmap _picture;
	private String _address;
	private String _phone;

	/**
	 * @return the _lastName
	 */
	public String getLastName() {
		return _lastName;
	}

	/**
	 * @param _lastName
	 *            the _lastName to set
	 */
	public void setLastName(String _lastName) {
		this._lastName = _lastName;
	}

	/**
	 * @return the _firstName
	 */
	public String getFirstName() {
		return _firstName;
	}

	/**
	 * @param _firstName
	 *            the _firstName to set
	 */
	public void setFirstName(String _firstName) {
		this._firstName = _firstName;
	}

	/**
	 * @return the _compagnyName
	 */
	public String getCompagnyName() {
		return _compagnyName;
	}

	/**
	 * @param _compagnyName
	 *            the _compagnyName to set
	 */
	public void setCompagnyName(String _compagnyName) {
		this._compagnyName = _compagnyName;
	}

	/**
	 * @return the _emailAddress
	 */
	public String getEmailAddress() {
		return _emailAddress;
	}

	/**
	 * @param _emailAddress
	 *            the _emailAddress to set
	 */
	public void setEmailAddress(String _emailAddress) {
		this._emailAddress = _emailAddress;
	}

	/**
	 * @return the _picture
	 */
	public Bitmap getPicture() {
		return _picture;
	}

	/**
	 * @param _picture
	 *            the _picture to set
	 */
	public void setPicture(Bitmap _picture) {
		this._picture = _picture;
	}

	/**
	 * @return the _address
	 */
	public String getAddress() {
		return _address;
	}

	/**
	 * @param _address
	 *            the _address to set
	 */
	public void setAddress(String _address) {
		this._address = _address;
	}

	/**
	 * @return the _phone
	 */
	public String getPhone() {
		return _phone;
	}

	/**
	 * @param _phone
	 *            the _phone to set
	 */
	public void setPhone(String _phone) {
		this._phone = _phone;
	}

	public boolean hasId(int id) {
		return _id == id;
	}

	@Override
	public boolean equals(Object o) {
		Client client = (Client) o;

		if (client == null)
			return false;

		return client._id == this._id;

	}

	private List<Note> notes = new ArrayList<Note>();

	public List<Note> getNotes() {
		return notes;
	}

	public void addNote(Note note) {
		notes.add(note);
	}

	@Override
	public String toString() {
		return _firstName + " " + _lastName;
	}

	public int getID() {
		return _id;
	}

	public void setID(int id) {
		_id = id;

	}

	private List<Facturation> _bills = new ArrayList<Facturation>();

	public List<Facturation> getBills() {
		return _bills;
	}

	public void addBill(Facturation newBill) {
		_bills.add(newBill);
	}

	private List<Communication> _communications = new ArrayList<Communication>();

	public List<Communication> getCommunications() {
		return _communications;
	}

	public void addCommunication(Communication communication) {
		_communications.add(communication);
	}

	public void removeNote(Note note) {
		notes.remove(note);
	}
}
