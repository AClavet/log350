package com.etsmtl.gestionrapide.Model;

/**
 * 
 * This class represent a Note in the context of the application
 * GestionRapide
 * 
 * @author: Jean-Christophe Fortin
 */

import java.util.Date;

public class Note {

	private String _subject;
	private String _body;
	private Date _date;
	private Client _client;

	/**
	 * @return the _subject
	 */
	public String getSubject() {
		return _subject;
	}

	/**
	 * @param _subject
	 *            the _subject to set
	 */
	public void setSubject(String _subject) {
		this._subject = _subject;
	}

	/**
	 * @return the _body
	 */
	public String getBody() {
		return _body;
	}

	/**
	 * @param _body
	 *            the _body to set
	 */
	public void setBody(String _body) {
		this._body = _body;
	}

	/**
	 * @return the _date
	 */
	public Date getDate() {
		return _date;
	}

	/**
	 * @param _date
	 *            the _date to set
	 */
	public void setDate(Date _date) {
		this._date = _date;
	}

	/**
	 * @return the _client
	 */
	public Client getClient() {
		return _client;
	}

	/**
	 * @param _client the _client to set
	 */
	public void setClient(Client _client) {
		this._client = _client;
		_client.addNote(this);
	}

}
