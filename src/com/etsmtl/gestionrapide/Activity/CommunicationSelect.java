package com.etsmtl.gestionrapide.Activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Activity.ArrayAdapter.CommunicationSelectAdapter;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Communication;
import com.etsmtl.gestionrapide.Model.Facturation;
import com.etsmtl.gestionrapide.Model.Note;

import android.app.Activity;
import android.os.Bundle;
import android.sax.TextElementListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class CommunicationSelect extends Activity {

	private Button btn_save;
	private Button btn_del;
	private Spinner dd_type;
	private Spinner dd_clients;
	private TextView txt_body;
	private DatePicker dp_date;
	private Communication _communication;
	private Date _date = new Date();

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communication);
		initComInfos();
		initUiComponents();

		initEventListner();
	}

	private void initEventListner() {
		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean isNew = (_communication == null);

				if (isNew) {
					Client client = (Client) dd_clients
							.getItemAtPosition(dd_clients
									.getSelectedItemPosition());

					if (client == null)
						return;

					String type = (String) dd_type.getItemAtPosition(dd_type
							.getSelectedItemPosition());

					_communication = new Communication();
					_communication.setDetails(txt_body.getText().toString());

					_date.setYear(dp_date.getYear());
					_date.setMonth(dp_date.getMonth());
					_date.setDate(dp_date.getDayOfMonth());

					_communication.setDate(_date);

					_communication.setType(type);
					_communication.setClient(client);

					client.addCommunication(_communication);
				} else {
					_communication.setDetails(txt_body.getText().toString());

					_date.setYear(dp_date.getYear());
					_date.setMonth(dp_date.getMonth());
					_date.setDate(dp_date.getDayOfMonth());

					_communication.setDate(_date);
				}

				if (isNew) {
					finish();
				} else {
					CharSequence text = "Saved.";
					int duration = Toast.LENGTH_SHORT;
					Toast toast = Toast.makeText(getApplicationContext(), text,
							duration);
					toast.show();
				}
			}
		});
		btn_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Client client = _communication.getClient();
				List<Communication> comms = client.getCommunications();
				comms.remove(_communication);
				finish();
				
			}
		});
	}

	private void initUiComponents() {
		txt_body = (TextView) findViewById(R.id.txt_body);
		btn_save = (Button) findViewById(R.id.btn_save);
		btn_del = (Button) findViewById(R.id.btn_del);
		dd_type = (Spinner) findViewById(R.id.dd_type);
		dd_clients = (Spinner) findViewById(R.id.dd_clients);
		dp_date = (DatePicker) findViewById(R.id.dp_date);
		List<String> types = new ArrayList<String>();

		if (_communication != null) {
			if (Controller.getInstance().getActiveClient() != null) {
				((TextView) findViewById(R.id.textView2))
						.setVisibility(View.INVISIBLE);
				dd_clients.setVisibility(View.INVISIBLE);
			}

		} else {
			btn_del.setVisibility(View.INVISIBLE);
		}

		if (_communication != null) {
			dp_date.updateDate(_communication.getDate().getYear(),
					_communication.getDate().getMonth(), _communication
							.getDate().getDate());
		} else {
			dp_date.updateDate(_date.getYear(), _date.getMonth(),
					_date.getDate());
		}

		types.add("SMS");
		types.add("Phone call");

		SpinnerAdapter type_adapter = new ArrayAdapter<String>(
				CommunicationSelect.this, android.R.layout.simple_spinner_item,
				types);
		dd_type.setAdapter(type_adapter);

		List<Client> clients = Controller.getInstance().getClients();

		SpinnerAdapter client_adapter = new ArrayAdapter<Client>(
				CommunicationSelect.this, android.R.layout.simple_spinner_item,
				clients);
		dd_clients.setAdapter(client_adapter);

	}

	private void initComInfos() {
		_communication = Controller.getInstance().getActiveCommunication();

	}
}