package com.etsmtl.gestionrapide.Activity;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Facturation;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class FacturationActivity extends Activity {

	private TextView txt_name;
	private ToggleButton tbtn_chronoSwitch;
	private TextView txt_chrono;
	private Button btn_save;
	private Button btn_send;
	private Button btn_del;
	private Spinner dd_clients;
	private Facturation _bill;
	private boolean _isNew;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facturation_information);

		initUiComponents();
		initEventListner();

		getFacture();
		refreshBillInfo();
	}

	private void getFacture() {
		_bill = Controller.getInstance().getActiveTimeEntry();
		if (_bill == null) {
			_isNew = true;
			_bill = new Facturation();
			Client currentClient = Controller.getInstance().getActiveClient();
			if (currentClient != null)
				_bill.setClient(currentClient);
		} else {
			_isNew = false;
		}
	}

	OnClickListener mStartListener = new OnClickListener() {
		public void onClick(View v) {
			if (startTime == 0L) {
				startTime = System.currentTimeMillis();
				handler.removeCallbacks(updateTimeTask);
				handler.postDelayed(updateTimeTask, 100);
			}
		}
	};

	long startTime = 0;
	private Runnable updateTimeTask = new Runnable() {
		public void run() {
			final long start = startTime;
			Calendar cal = Calendar.getInstance();
			long millis = cal.getTimeInMillis() - start;
			int seconds = (int) (millis / 1000);
			int minutes = seconds / 60;
			seconds = seconds % 60;

			if (seconds < 10) {
				txt_chrono.setText("" + minutes + ":0" + seconds);
			} else {
				txt_chrono.setText("" + minutes + ":" + seconds);
			}

			handler.postDelayed(this, 1000);
		}
	};

	private void initEventListner() {
		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (_isNew) {
					if (Controller.getInstance().getActiveClient() == null) {
						Client client = (Client) dd_clients
								.getItemAtPosition(dd_clients
										.getSelectedItemPosition());
						_bill.setClient(client);
						client.addBill(_bill);
					} else {
						Controller.getInstance().getActiveClient()
								.addBill(_bill);
					}
				}
				_bill.setName(txt_name.getText().toString());

				if (_isNew) {
					finish();
				} else {
					CharSequence text = "Saved.";
					int duration = Toast.LENGTH_SHORT;
					Toast toast = Toast.makeText(getApplicationContext(), text,
							duration);
					toast.show();
				}

			}
		});

		tbtn_chronoSwitch
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {

						if (startTime == 0L) {
							startTime = System.currentTimeMillis();
							handler.removeCallbacks(updateTimeTask);
							handler.postDelayed(updateTimeTask, 100);
						} else {
							if (handler != null && _bill != null) {
								handler.removeCallbacks(updateTimeTask);
								_bill.addTime((Calendar.getInstance()
										.getTimeInMillis() - startTime));
								startTime = 0;
							}
						}
					}
				});
		
		btn_del.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Client client = _bill.getClient();
				List<Facturation> bills = client.getBills();
				bills.remove(_bill);
				finish();
				
			}
		});
		
		btn_send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				CharSequence text = "Invoice sent to client.";
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(getApplicationContext(), text,
						duration);
				toast.show();
				
			}
		});
	}

	private Handler handler = new Handler();

	private void refreshBillInfo() {
		if (Controller.getInstance().getActiveClient() != null) {
			dd_clients.setVisibility(View.INVISIBLE);
			TextView label = (TextView) findViewById(R.id.textView2);
			label.setVisibility(View.INVISIBLE);
		}
		
		if(_isNew)
		{
			btn_send.setVisibility(View.INVISIBLE);
			btn_del.setVisibility(View.INVISIBLE);
		}

		txt_name.setText(_bill.getName());
	}

	private void initUiComponents() {
		txt_name = (TextView) findViewById(R.id.txt_name);
		txt_chrono = (TextView) findViewById(R.id.txt_chrono);
		btn_save = (Button) findViewById(R.id.btn_save);
		btn_del = (Button) findViewById(R.id.btn_del);
		btn_send = (Button) findViewById(R.id.btn_send);
		tbtn_chronoSwitch = (ToggleButton) findViewById(R.id.tbtn_chronoSwitch);
		dd_clients = (Spinner) findViewById(R.id.dd_clients);

		List<Client> clients = Controller.getInstance().getClients();

		SpinnerAdapter adapter = new ArrayAdapter<Client>(
				FacturationActivity.this, android.R.layout.simple_spinner_item,
				clients);
		dd_clients.setAdapter(adapter);
	}
}
