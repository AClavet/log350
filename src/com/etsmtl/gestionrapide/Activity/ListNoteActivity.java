package com.etsmtl.gestionrapide.Activity;

import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Activity.ArrayAdapter.NotesAdapter;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ListView;

public class ListNoteActivity extends Activity {
	/** Called when the activity is first created. */

	private List<Note> _notes;

	private Button btn_addNote;
	private ListView _noteList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_note);

		initNoteList();
		initUiComponents();
		initEventListner();

		refreshNotesList();
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshNotesList();
	}

	private void refreshNotesList() {
		_notes = Controller.getInstance().getAllNotes();
		if (_notes != null && _notes.size() > 0) {
			_noteList.setAdapter(new NotesAdapter(this, R.layout.note_entry,
					_notes));
		}
	}

	private void initEventListner() {
		_noteList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Note note = (Note) parent.getItemAtPosition(position);
				if (note != null) {
					Intent intent = new Intent(ListNoteActivity.this,
							EditNoteActivity.class);
					Controller.getInstance().setActiveNote(note);
					startActivity(intent);
				}
			}
		});

		btn_addNote.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ListNoteActivity.this,
						EditNoteActivity.class);
				Controller.getInstance().setActiveNote(null);
				startActivity(intent);
			}
		});
	}

	private void initUiComponents() {
		btn_addNote = (Button) findViewById(R.id.btn_addNote);
		_noteList = (ListView) findViewById(R.id.lv_notes);

	}

	private void initNoteList() {
		_notes = Controller.getInstance().getAllNotes();
	}
}