package com.etsmtl.gestionrapide.Activity;

import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Activity.ArrayAdapter.ClientsAdapter;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ListClientActivity extends Activity {

	private Button _addContact;
	private ListView _clientList;
	private List<Client> _clients;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_client);

		initClientList();
		initUiComponents();
		initEventListner();

		refreshClientList();
	}

	private void initEventListner() {

		// BTN_ADD_CONTACTS_LISTNER
		_addContact.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				launchClientAdd();
			}
		});

		// ITEM_CLIENT_LISTNER
		_clientList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				
				launchClientInformationIntent((Client) parent
						.getItemAtPosition(position));
			}
		});

	}

	private void initUiComponents() {
		_addContact = (Button) findViewById(R.id.addContactButton);
		_clientList = (ListView) findViewById(R.id.lv_clients);
	}

	private void initClientList() {
		_clients = Controller.getInstance().getClients();
	}

	private void refreshClientList() {
		if (_clients.size() != 0) {
			_clientList.setAdapter(new ClientsAdapter(this,
					R.layout.client_entry, _clients));
		}
	}

	private void launchClientAdd() {
		Intent intent = new Intent(ListClientActivity.this,
				ClientInformation.class);
		Controller.getInstance().setActiveClient(null);
		startActivity(intent);
	}

	private void launchClientInformationIntent(Client client) {
		Intent intent = new Intent(ListClientActivity.this,
				ClientActivity.class);
		Controller.getInstance().setActiveClient(client);

		startActivity(intent);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		refreshClientList();
	}

}
