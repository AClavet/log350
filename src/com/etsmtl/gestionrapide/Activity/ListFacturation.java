package com.etsmtl.gestionrapide.Activity;

import java.util.ArrayList;
import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Activity.ArrayAdapter.FacturationAdapter;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Facturation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class ListFacturation extends Activity {

	private List<Facturation> _bills;
	private ListView _billList;
	private Button btn_addFacturation;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facturation_list);

		initFacturationList();
		initUiComponents();
		initEventListner();

		refreshFacturationList();
	}

	private void refreshFacturationList() {
		initFacturationList();
		if (_bills != null) {
			_billList.setAdapter(new FacturationAdapter(this,
					R.layout.facturation_entry, _bills));
		}
	}

	private void initEventListner() {
		btn_addFacturation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ListFacturation.this,
						FacturationActivity.class);
				Controller.getInstance().setActiveTimeEntry(null);
				startActivity(intent);
			}
		});

		_billList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Facturation bill = (Facturation) parent
						.getItemAtPosition(position);

				if (bill != null) {
					Intent intent = new Intent(ListFacturation.this,
							FacturationActivity.class);
					Controller.getInstance().setActiveTimeEntry(bill);
					startActivity(intent);
				}
			}
		});

	}

	private void initUiComponents() {
		_billList = (ListView) findViewById(R.id.lv_facturation);
		btn_addFacturation = (Button) findViewById(R.id.btn_addFacturation);
	}

	private void initFacturationList() {
		Client client = Controller.getInstance().getActiveClient();

		if (client != null) {
			_bills = client.getBills();
		}
		else{
			_bills = Controller.getInstance().getAllBills();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshFacturationList();
	}
}
