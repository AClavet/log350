package com.etsmtl.gestionrapide.Activity;

import java.util.Date;
import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class EditNoteActivity extends Activity {

	private Note _note;

	private Button btn_save;
	private Spinner dd_clients;
	private TextView txt_subject;
	private TextView txt_body;
	private Button btn_del;
	private boolean isNew;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.note);
		getNote();
		isNew = (_note == null);
		
		initUiComponents();
		initEventListner();

		
		refreshNodeInfos();

	}

	private void initEventListner() {
		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				

				if (isNew) {
					Client client = (Client) dd_clients
							.getItemAtPosition(dd_clients
									.getSelectedItemPosition());

					if (client == null)
						return;

					_note = new Note();
					_note.setBody(txt_body.getText().toString());
					_note.setDate(new Date());
					_note.setSubject(txt_subject.getText().toString());

					_note.setClient(client);
				} else {
					_note.setBody(txt_body.getText().toString());
					_note.setDate(new Date());
				}

				if (isNew) {
					finish();
				} else {
					CharSequence text = "Saved.";
					int duration = Toast.LENGTH_SHORT;
					Toast toast = Toast.makeText(getApplicationContext(), text,
							duration);
					toast.show();
				}
			}
		});

		btn_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (_note != null) {
					Client client = _note.getClient();
					if (client != null) {
						client.removeNote(_note);
						finish();
					}
				}

			}
		});

	}

	private void initUiComponents() {
		btn_save = (Button) findViewById(R.id.btn_save);
		dd_clients = (Spinner) findViewById(R.id.dd_clients);

		txt_body = (TextView) findViewById(R.id.txt_body);
		txt_subject = (TextView) findViewById(R.id.txt_subject);

		btn_del = (Button) findViewById(R.id.btn_del);

		if (isNew) {
			btn_del.setVisibility(View.INVISIBLE);
		}
	}

	private void refreshNodeInfos() {
		if (_note != null) {
			txt_body.setText(_note.getBody());
			txt_subject.setText(_note.getSubject());
		}

		List<Client> clients = Controller.getInstance().getClients();

		SpinnerAdapter adapter = new ArrayAdapter<Client>(
				EditNoteActivity.this, android.R.layout.simple_spinner_item,
				clients);
		dd_clients.setAdapter(adapter);

	}

	private void getNote() {
		_note = Controller.getInstance().getActiveNote();
	}
}
