package com.etsmtl.gestionrapide.Activity;

import com.etsmtl.gestionrapide.R;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class ClientActivity extends TabActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.client);

		Resources res = getResources(); // Resource object to get Drawables
		TabHost tabHost = getTabHost(); // The activity TabHost
		TabHost.TabSpec spec; // Reusable TabSpec for each tab
		Intent intent; // Reusable Intent for each tab

		// ADD ONE TAB (REPEAT AS YOU WISH)
		intent = new Intent().setClass(this, ClientInformation.class);
		spec = tabHost.newTabSpec("client").setIndicator("Client",res.getDrawable(R.drawable.client))
				.setContent(intent);
		tabHost.addTab(spec);

		intent = new Intent().setClass(this, ListNoteActivity.class);
		spec = tabHost.newTabSpec("notes").setIndicator("Notes",res.getDrawable(R.drawable.note))
				.setContent(intent);
		tabHost.addTab(spec);
		
		intent = new Intent().setClass(this, ListFacturation.class);
		spec = tabHost.newTabSpec("Facturation").setIndicator("Bills",res.getDrawable(R.drawable.facture))
				.setContent(intent);
		tabHost.addTab(spec);
		
		intent = new Intent().setClass(this, ListCommunication.class);
		spec = tabHost.newTabSpec("Communication").setIndicator("Communications",res.getDrawable(R.drawable.communication))
				.setContent(intent);
		tabHost.addTab(spec);

		tabHost.setCurrentTab(0);
	}
}