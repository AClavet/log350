package com.etsmtl.gestionrapide.Activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Activity.ArrayAdapter.CommunicationsAdapter;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Communication;
import com.etsmtl.gestionrapide.Model.Facturation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ListCommunication extends Activity {

	private List<Communication> _communications;
	private ListView commList;
	private Button btn_addComm;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.communication_list);

		initCommList();
		initUiComponents();
		initEventListner();

		refreshCommList();
	}

	private void refreshCommList() {
		if (_communications.size() != 0) {
			commList.setAdapter(new CommunicationsAdapter(this,
					R.layout.communication_entry, _communications,
					getResources()));
		}
	}

	private void initEventListner() {
		btn_addComm.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ListCommunication.this,
						CommunicationSelect.class);
				Controller.getInstance().setActiveCommunication(null);
				startActivity(intent);
			}
		});

		commList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Communication com = (Communication) parent
						.getItemAtPosition(position);

				if (com != null) {
					Intent intent = new Intent(ListCommunication.this,
							CommunicationSelect.class);
					Controller.getInstance().setActiveCommunication(com);
					startActivity(intent);
				}
			}
		});
	}

	private void initUiComponents() {
		commList = (ListView) findViewById(R.id.lv_communication);
		btn_addComm = (Button) findViewById(R.id.btn_addCommunication);
	}

	private void initCommList() {
		Client client = Controller.getInstance().getActiveClient();

		if (client != null) {
			_communications = client.getCommunications();
		}
		else{
			_communications = Controller.getInstance().getAllCommunications();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		initCommList();
		refreshCommList();
	}
}
