package com.etsmtl.gestionrapide.Activity;

import java.lang.reflect.Type;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Core.Controller;
import com.etsmtl.gestionrapide.Model.Client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

public class ClientInformation extends Activity {

	protected static final int SELECT_IMAGE = 0;

	private Client _client;

	private TextView txt_firstName;
	private TextView txt_lastName;
	private TextView txt_address;
	private TextView txt_email;
	private TextView txt_phone;
	private QuickContactBadge qcb_picture;
	private Button btn_save;
	private Button btn_picture;
	private Button btn_del;
	boolean isNew;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.client_information);
		GetClient();
		isNew = (_client == null);
		initUiComponents();
		initEventListner();

		
		ShowClientInformation();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == SELECT_IMAGE)
			if (resultCode == Activity.RESULT_OK) {
				Uri selectedImage = data.getData();
				// TODO Do something with the select image URI
			}
	}

	private void initEventListner() {

		// BTN_PICTURE
		btn_picture.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(
						new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI),
						SELECT_IMAGE);
			}
		});

		// BTN_SAVE
		btn_save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				

				if (isNew) {
					_client = new Client();
				}

				_client.setFirstName(txt_firstName.getText().toString());
				_client.setLastName(txt_lastName.getText().toString());
				_client.setPhone(txt_phone.getText().toString());
				_client.setEmailAddress(txt_email.getText().toString());
				// TODO : Picture

				if (isNew) {
					Controller.getInstance().addClient(_client);
					finish();
				} else {
					CharSequence text = "Saved.";
					int duration = Toast.LENGTH_SHORT;
					Toast toast = Toast.makeText(getApplicationContext(), text,
							duration);
					toast.show();
				}
			}
		});

		btn_del.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (_client == null) {
					return;
				}
				Controller.getInstance().removeClient(_client);
				finish();
			}
		});
	}

	private void initUiComponents() {
		txt_firstName = (TextView) findViewById(R.id.txt_firstName);
		txt_lastName = (TextView) findViewById(R.id.txt_lastName);
		txt_address = (TextView) findViewById(R.id.txt_address);
		txt_phone = (TextView) findViewById(R.id.txt_phone);
		txt_email = (TextView) findViewById(R.id.txt_email);
		qcb_picture = (QuickContactBadge) findViewById(R.id.qcb_picture);
		btn_picture = (Button) findViewById(R.id.btn_picture);
		btn_save = (Button) findViewById(R.id.btn_save);
		btn_del = (Button) findViewById(R.id.btn_del);
		
		if(isNew)
		{
			btn_del.setVisibility(View.INVISIBLE);
		}
		
		
	}

	private void ShowClientInformation() {
		if (_client != null) {
			txt_firstName.setText(_client.getFirstName());
			txt_lastName.setText(_client.getLastName());
			txt_address.setText(_client.getAddress());
			txt_phone.setText(_client.getPhone());
			txt_email.setText(_client.getEmailAddress());
			qcb_picture.setImageBitmap(_client.getPicture());
		} else {
			btn_del.setVisibility(4);
		}
	}

	private void GetClient() {
		_client = Controller.getInstance().getActiveClient();
	}
}