package com.etsmtl.gestionrapide.Activity.ArrayAdapter;

import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Activity.EditNoteActivity;
import com.etsmtl.gestionrapide.Activity.ListNoteActivity;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Note;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class NotesAdapter extends ArrayAdapter<Note> {

	int resource;
	String response;
	Context context;

	// Initialize adapter
	public NotesAdapter(Context context, int resource, List<Note> items) {
		super(context, resource, items);
		this.resource = resource;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout noteEntryView;
		Note note = getItem(position);

		// Inflate the view
		if (convertView == null) {
			noteEntryView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi;
			vi = (LayoutInflater) getContext().getSystemService(inflater);
			vi.inflate(resource, noteEntryView, true);
		} else {
			noteEntryView = (LinearLayout) convertView;
		}

		if (noteEntryView != null) {

			TextView txt_noteName = (TextView) noteEntryView
					.findViewById(R.id.txt_noteName);
			TextView txt_clientName = (TextView) noteEntryView
					.findViewById(R.id.txt_clientName);
			TextView txt_date = (TextView) noteEntryView
					.findViewById(R.id.txt_dateModif);

			if (note != null) {
				txt_noteName.setText(note.getSubject());

				if (note.getClient() != null) {
					txt_clientName.setText(note.getClient().getFirstName()
							+ " " + note.getClient().getLastName());
				} else {
					txt_clientName.setText("");
				}
				txt_date.setText(note.getDate().toLocaleString());
			}
		}

		return noteEntryView;
	}

}