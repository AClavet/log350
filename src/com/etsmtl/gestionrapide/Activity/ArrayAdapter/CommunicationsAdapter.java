package com.etsmtl.gestionrapide.Activity.ArrayAdapter;

import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Communication;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class CommunicationsAdapter extends ArrayAdapter<Communication> {

	int resource;
	String response;
	Context context;
	private final Resources res;

	// Initialize adapter
	public CommunicationsAdapter(Context context, int resource, List<Communication> items,Resources res) {
		super(context, resource, items);
		this.resource = resource;
		this.res = res;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout communicationEntryView;
		Communication comm = getItem(position);

		// Inflate the view
		if (convertView == null) {
			communicationEntryView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi;
			vi = (LayoutInflater) getContext().getSystemService(inflater);
			vi.inflate(resource, communicationEntryView, true);
		} else {
			communicationEntryView = (LinearLayout) convertView;
		}
		
		QuickContactBadge qbc_picture = (QuickContactBadge)communicationEntryView.findViewById(R.id.qcb_picture);
		TextView txt_commType = (TextView)communicationEntryView.findViewById(R.id.txt_CommType);
		TextView txt_date = (TextView)communicationEntryView.findViewById(R.id.txt_DateComm);
		
		txt_commType.setText(comm.getType());
		txt_date.setText(comm.getDate().toLocaleString());
		if(comm.getType()!=null && comm.getType().equals("SMS"))
		{
			qbc_picture.setImageDrawable(res.getDrawable(R.drawable.sms));
		}
		else
		{
			qbc_picture.setImageDrawable(res.getDrawable(R.drawable.call));
		}

		return communicationEntryView;
	}
}