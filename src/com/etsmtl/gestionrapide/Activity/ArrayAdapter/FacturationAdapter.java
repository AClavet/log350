package com.etsmtl.gestionrapide.Activity.ArrayAdapter;

import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Model.Client;
import com.etsmtl.gestionrapide.Model.Facturation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FacturationAdapter extends ArrayAdapter<Facturation> {

	int resource;
	String response;
	Context context;

	// Initialize adapter
	public FacturationAdapter(Context context, int resource,
			List<Facturation> items) {
		super(context, resource, items);
		this.resource = resource;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout facturationEntryView;
		Facturation facturation = getItem(position);

		// Inflate the view
		if (convertView == null) {
			facturationEntryView = new RelativeLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi;
			vi = (LayoutInflater) getContext().getSystemService(inflater);
			vi.inflate(resource, facturationEntryView, true);
		} else {
			facturationEntryView = (RelativeLayout) convertView;
		}

		TextView txt_facturationName = (TextView) facturationEntryView
				.findViewById(R.id.txt_facturationName);
		TextView txt_isOn = (TextView) facturationEntryView
				.findViewById(R.id.txt_isOn);
		TextView txt_timeElapse = (TextView) facturationEntryView
				.findViewById(R.id.txt_timeElapse);

		txt_facturationName.setText(facturation.getName());
		txt_isOn.setText(facturation.isOn() ? "Still Billing" : "Finished");
		long timeElapsed = facturation.getBilledTimeInMs();
		int seconds = (int) (timeElapsed / 1000);
		int minutes = seconds / 60;
		seconds = seconds % 60;
		
		if (seconds < 10) {
			txt_timeElapse.setText("" + minutes + ":0" + seconds);
		} else {
			txt_timeElapse.setText("" + minutes + ":" + seconds);
		}

		return facturationEntryView;
	}

}