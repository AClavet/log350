package com.etsmtl.gestionrapide.Activity.ArrayAdapter;

import java.util.List;

import com.etsmtl.gestionrapide.R;
import com.etsmtl.gestionrapide.Model.Client;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class ClientsAdapter extends ArrayAdapter<Client> {

	int resource;
	String response;
	Context context;

	// Initialize adapter
	public ClientsAdapter(Context context, int resource, List<Client> items) {
		super(context, resource, items);
		this.resource = resource;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout clientEntryView;
		Client client = getItem(position);

		// Inflate the view
		if (convertView == null) {
			clientEntryView = new LinearLayout(getContext());
			String inflater = Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater vi;
			vi = (LayoutInflater) getContext().getSystemService(inflater);
			vi.inflate(resource, clientEntryView, true);
		} else {
			clientEntryView = (LinearLayout) convertView;
		}

		TextView txt_clientName = (TextView) clientEntryView
				.findViewById(R.id.txt_clientName);
		QuickContactBadge qcb_clientPicture = (QuickContactBadge) clientEntryView
				.findViewById(R.id.qcb_picture);

		txt_clientName.setText(client.getFirstName() + " "
				+ client.getLastName());
		qcb_clientPicture.setImageBitmap(client.getPicture());

		return clientEntryView;
	}

}
