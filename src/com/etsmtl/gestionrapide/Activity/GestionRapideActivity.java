package com.etsmtl.gestionrapide.Activity;

import com.etsmtl.gestionrapide.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class GestionRapideActivity extends Activity {
	/** Called when the activity is first created. */

	private ImageButton ibtn_clients;
	private ImageButton ibtn_facturation;
	private ImageButton ibtn_communication;
	private ImageButton ibtn_note;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initUiComponents();
		initEventListner();
	}

	private void initEventListner() {
		ibtn_clients.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(GestionRapideActivity.this,
						ListClientActivity.class);
				startActivity(intent);
			}
		});

		ibtn_note.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(GestionRapideActivity.this,
						ListNoteActivity.class);
				startActivity(intent);
			}
		});

		ibtn_facturation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(GestionRapideActivity.this,
						ListFacturation.class);
				startActivity(intent);
			}
		});

		ibtn_communication.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(GestionRapideActivity.this,
						ListCommunication.class);
				startActivity(intent);
			}
		});
	}

	private void initUiComponents() {
		ibtn_clients = (ImageButton) findViewById(R.id.ibtn_clients);
		ibtn_facturation = (ImageButton) findViewById(R.id.ibtn_facturations);
		ibtn_communication = (ImageButton) findViewById(R.id.ibtn_comm);
		ibtn_note = (ImageButton) findViewById(R.id.ibtn_notes);
	}
}